# French localized [Curzy's JSON Resume theme](https://github.com/Curzy/jsonresume-theme-curzy)

Basically a French translation of Curzy's JSON Resume theme.

## License

[MIT](https://choosealicense.com/licenses/mit/)
  
## Acknowledgements

 - [jsonresume-theme-curzy](https://github.com/Curzy/jsonresume-theme-curzy)
 - [jsonresume-theme-stackoverflow](https://github.com/phoinixi/jsonresume-theme-stackoverflow)
 - [JSON Resume](https://jsonresume.org/)
 - [HackMyResume](https://github.com/hacksalot/HackMyResume)

