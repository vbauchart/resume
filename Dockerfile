FROM node:lts

RUN apt-get update && \
    apt-get install -y --no-install-recommends chromium git openssh-client

RUN mkdir /jsonresume

WORKDIR /jsonresume

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true

ENV CHROMIUM_PATH /usr/bin/chromium-browser

ENV PUPPETEER_EXECUTABLE_PATH=${CHROMIUM_PATH}

ENV RESUME_PUPPETEER_NO_SANDBOX=1

RUN npm install -g resume-cli

RUN npm install \
        jsonresume-theme-even \
        jsonresume-theme-flat \
        jsonresume-theme-elegant \
        jsonresume-theme-paper

COPY jsonresume-theme-curzy-fr /tmp/jsonresume-theme-curzy-fr

RUN npm install /tmp/jsonresume-theme-curzy-fr

ENTRYPOINT []

CMD "resume --help"